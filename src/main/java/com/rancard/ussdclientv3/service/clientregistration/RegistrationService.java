package com.rancard.ussdclientv3.service.clientregistration;

import com.rancard.ussdclientv3.controller.UssdRequest;
import com.rancard.ussdclientv3.controller.UssdResponse;
import com.rancard.ussdclientv3.model.*;
import com.rancard.ussdclientv3.model.registration.Registration;
import com.rancard.ussdclientv3.service.paymentmethod.PaymentMethodService;
import com.rancard.ussdclientv3.service.request.UssdClientService;
import com.rancard.ussdclientv3.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    private PaymentMethodService paymentMethodService;


    RegistrationService(PaymentMethodService paymentMethodService){
        this.paymentMethodService = paymentMethodService;
    }

    @Autowired
    private UssdClientService ussdClientService;

    @Value("${response.welcome.message}")
    protected String WELCOME_MESSAGE;

    @Value("${response.first.name}")
    protected String FIRST_NAME_MESSAGE;

    @Value("${response.last.name}")
    protected String LAST_NAME_MESSAGE;

    @Value("${response.request.valid.option}")
    protected String REQUEST_VALID_OPTION;

    @Value("${response.request.valid.input}")
    protected String REQUEST_VALID_INPUT;

    @Value("${response.back}")
    protected String BACK;

//    @Value("${response.product.selection}")
    @Value("${response.product.selection}")
    protected String SELECT_PRODUCT;

    @Value("${response.back.or.message}")
    protected String BACK_OR_SKIP;

    @Value("${response.other.name}")
    protected String OTHER_NAME_MESSAGE;

    @Value("${response.phone.number}")
    protected String PHONE_NUMBER;

    @Value("${response.id.type}")
    protected String ID_TYPE;

    @Value("${response.id.number}")
    protected String ID_NUMBER;

    @Value("${response.another.transaction}")
    protected String ANOTHER_TRANSACTION;

    @Value("${response.payment.method}")
    protected String PAYMENT_METHOD;

    @Value("${response.registration.deposit.amount}")
    protected String REGISTRATION_DEPOSIT_AMOUNT;

    private Logger logger = LoggerFactory.getLogger(RegistrationService.class);


    public UssdResponse registerClient(UssdRequest ussdRequest, String sessionId, Session session,UssdResponse ussdResponse){
        logger.info("["+sessionId+"] about to switch on registration menu for agent with msisdn : " + ussdRequest.getMsisdn());
        Client client = session.getClientProperties();
        Agent agent = session.getAgentProperties();

        if(client == null){
            client = new Client();
        }

        if(agent == null){
            agent = new Agent();
            agent.setMsisdn(ussdRequest.getMsisdn());
        }

        MENU_OPTION menuOption = MENU_OPTION.fromChoice(ussdRequest.getMessage());
        switch (session.getMenuLevel()){
            case "productSelection":
                logger.info("["+sessionId+"] user selected product option "+ussdRequest.getMessage());
                if(MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    logger.info("["+sessionId+"] user chose to go to previous menu item. from registration " +
                            "product selection sub menu");
                    ussdResponse.setMessage(WELCOME_MESSAGE);
                    session.setMenuLevel("agentDefaultMenu");
                    session.setSubMenuLevel(null);
                    ussdResponse.setContinueSession(true);
                }else{
                    PRODUCT selectedProduct = PRODUCT.fromChoice(ussdRequest.getMessage().trim());
                    if(selectedProduct != null){
                        logger.info("["+sessionId+"] user selected product option"
                                +ussdRequest.getMessage());
                        client.setProduct(selectedProduct);
                        session.setMenuLevel("firstName");
                        session.setClientProperties(client);
                        ussdResponse.setContinueSession(true);
                        ussdResponse.setMessage(FIRST_NAME_MESSAGE+BACK);
                    }else{
                        logger.info("["+sessionId+"] user selected product invalid product option"
                                +ussdRequest.getMessage());
                        ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+SELECT_PRODUCT+BACK);
                        session.setMenuLevel("productSelection");
                        ussdResponse.setContinueSession(true);

                    }
                }
                break;
            case  "firstName":
                 if(menuOption == MENU_OPTION.ZERO){
                ussdResponse.setMessage(SELECT_PRODUCT+BACK);
                session.setMenuLevel("productSelection");
                ussdResponse.setContinueSession(true);
            } else if(ussdRequest.getMessage().matches("^[a-zA-Z \\.\\-]+$")){
                logger.info("["+sessionId+"] setting client first name to "+ussdRequest.getMessage());
                ussdResponse.setMessage(LAST_NAME_MESSAGE);
                client.setFirstName(ussdRequest.getMessage().trim());
                session.setClientProperties(client);
                ussdResponse.setContinueSession(true);
                logger.info("["+sessionId+"] Session state : " + session.toString());
                session.setMenuLevel("lastName");
                break;
            }else{
                logger.info("["+sessionId+"] user tried to input invalid name "+ussdRequest.getMessage());
                ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+FIRST_NAME_MESSAGE+BACK);
                ussdResponse.setContinueSession(true);
                session.setMenuLevel("firstName");
            }
                break;
            case "lastName":
                if(menuOption == MENU_OPTION.ZERO){
                    ussdResponse.setMessage(FIRST_NAME_MESSAGE+BACK);
                    ussdResponse.setContinueSession(true);
                }else if(ussdRequest.getMessage().matches("^[a-zA-Z \\.\\-]+$")){
                    logger.info("["+sessionId+"] setting client last name to "+ussdRequest.getMessage());
                    client.setLastName(ussdRequest.getMessage().trim());
                    session.setClientProperties(client);
                    logger.info("["+sessionId+"] Session state : " + session.toString());
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(OTHER_NAME_MESSAGE+BACK_OR_SKIP);
                    session.setMenuLevel("otherNames");
                }else{
                    logger.info("["+sessionId+"] user tried to input invalid name "+ussdRequest.getMessage());
                    ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+LAST_NAME_MESSAGE+BACK);
                    ussdResponse.setContinueSession(true);
                    session.setMenuLevel("lastName");
                }
                break;
            case "otherNames":
                if(menuOption == MENU_OPTION.ZERO){
                    ussdResponse.setMessage(LAST_NAME_MESSAGE+BACK);
                    ussdResponse.setContinueSession(true);
                } else if(ussdRequest.getMessage().matches("^[a-zA-Z \\.\\-]+$") ||
                        ussdRequest.getMessage().equals("9")){
                    logger.info("["+sessionId+"] setting client other name to "+ussdRequest.getMessage());
                    client.setOtherName(ussdRequest.getMessage().equals("9") ? "" : ussdRequest.getMessage().trim());
                    session.setClientProperties(client);
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(PHONE_NUMBER+BACK);
                    logger.info("["+sessionId+"] Session state : " + session.toString());
                    session.setMenuLevel("phoneNumber");
                }else{
                    logger.info("["+sessionId+"] user tried to input invalid name "+ussdRequest.getMessage());
                    ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+OTHER_NAME_MESSAGE+BACK);
                    ussdResponse.setContinueSession(true);
                    session.setMenuLevel("otherNames");
                }
                break;
            case "phoneNumber":
                if(menuOption == MENU_OPTION.ZERO){
                    ussdResponse.setMessage(OTHER_NAME_MESSAGE+BACK);
                    ussdResponse.setContinueSession(true);
                } else if(Utils.isValidMsisdn(ussdRequest.getMessage())){
                    logger.info("["+sessionId+"] setting client phone number name to "+ussdRequest.getMessage());
                    client.setMsisdn(ussdRequest.getMessage());
                    session.setClientProperties(client);
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(ID_TYPE+BACK);
                    logger.info("["+sessionId+"] Session state : " + session.toString());
                    session.setMenuLevel("idType");
                }else{
                    logger.info("["+sessionId+"] user tried to input invalid msisdn : "+ussdRequest.getMessage());
                    ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n\n"+PHONE_NUMBER+BACK);
                    ussdResponse.setContinueSession(true);
                    session.setMenuLevel("phoneNumber");
                }
                break;
            case "idType":
                if(menuOption == MENU_OPTION.ZERO){
                    ussdResponse.setMessage(PHONE_NUMBER+BACK);
                    ussdResponse.setContinueSession(true);
                } else if(MENU_OPTION.fromChoice(ussdRequest.getMessage()) != null){
                    logger.info("["+sessionId+"] setting client id type to "+ID.fromChoice(ussdRequest.getMessage()));
                    client.setIdType(ID.fromChoice(ussdRequest.getMessage()));
                    session.setClientProperties(client);
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(ID_NUMBER+BACK);
                    logger.info("["+sessionId+"] Session state : " + session.toString());
                    session.setMenuLevel("idNumber");
                }else{
                    logger.info("["+sessionId+"] user tried to input invalid id type : "+ussdRequest.getMessage());
                    ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+ID_TYPE+BACK);
                    ussdResponse.setContinueSession(true);
                    session.setMenuLevel("idType");
                }
                break;
            case "idNumber":
                if(menuOption == MENU_OPTION.ZERO){
                    ussdResponse.setMessage(ID_TYPE+BACK);
                    ussdResponse.setContinueSession(true);
                } else {
                    logger.info("[" + sessionId + "] setting client id number to " + ussdRequest.getMessage());
                    client.setIdNumber(ussdRequest.getMessage());
                    session.setClientProperties(client);
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(PAYMENT_METHOD + BACK);
                    logger.info("["+sessionId+"] Session state : " + session.toString());
                    session.setMenuLevel("paymentMethod");
                }
                break;
            case "paymentMethod":
                PAYMENT_MEDIUM paymentMedium = PAYMENT_MEDIUM.fromChoice(ussdRequest.getMessage());
                boolean isValidPaymentMethodRequest = paymentMedium != null;
                if(MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    logger.info("["+sessionId+"] User chose to go to previous menu from paymentMethod sub menu");
                    ussdRequest.setMessage(ID_NUMBER+BACK);
                    session.setMenuLevel("idNumber");
                    ussdResponse.setContinueSession(true);
                }else if(isValidPaymentMethodRequest){
                    logger.info("["+sessionId+"] set client payment method to: "+paymentMedium.toString());
                    client.setPaymentMedium(paymentMedium);
                    if(paymentMedium == PAYMENT_MEDIUM.CASH){
                        ussdResponse.setMessage(String.format(REGISTRATION_DEPOSIT_AMOUNT,
                                PRODUCT.initialDeposit(client.getProduct()))+BACK);
                        session.setMenuLevel("cash");
                    }else if(paymentMedium == PAYMENT_MEDIUM.MOBILE_MONEY){
                        ussdResponse.setMessage("Not Yet Implemented"+BACK);
                        session.setMenuLevel("mobileMoney");
                    }else{
                        ussdResponse.setMessage("Not yet implemented"+BACK);
                        session.setMenuLevel("cheque");
                    }
                    session.setClientProperties(client);
                    ussdResponse.setContinueSession(true);
                }else{
                    logger.info("["+sessionId+"] user tried to input payment medium "+ussdRequest.getMessage());
                    ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+PAYMENT_METHOD+BACK);
                    ussdResponse.setContinueSession(true);
                    session.setMenuLevel("paymentMethod");
                }
                break;
            case "cash":
                ussdResponse = paymentMethodService.processCashPayment(session, sessionId, ussdResponse, ussdRequest);
                break;
            case "mobileMoney":
                break;
            case "cheque":
                break;
            case "confirmRegistration":
                logger.info("About ot confirm registration");
                if(MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    switch (client.getPaymentMedium()){
                        case CASH:
                            logger.info("["+sessionId+"] User chose to go to previous menu from confirm registration " +
                                    "cash  sub menu");
                            ussdResponse.setMessage(String.format(REGISTRATION_DEPOSIT_AMOUNT,
                                    PRODUCT.initialDeposit(client.getProduct()))+BACK);
                            break;
                        case CHEQUE:
                            break;
                        case MOBILE_MONEY:
                            break;
                    }
                    session.setMenuLevel("cash");
                    ussdResponse.setContinueSession(true);
                }else if(MENU_OPTION.ONE == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    logger.info("["+sessionId+"] User has confirmed registration of client"+client.toString());
                    Registration registrationDetails = new Registration(client);
                    registrationDetails.setAgentPhone(agent.getMsisdn());

                    ApiResponse apiResponse = ussdClientService.registerClient(registrationDetails, sessionId);

                    logger.info("["+sessionId+"] Response received : "+ apiResponse.toString());
                    ussdResponse.setContinueSession(false);
                    ussdResponse.setMessage(apiResponse.getMessage() + ANOTHER_TRANSACTION);
                }
            break;
        }

        return ussdResponse ;
    }

}
