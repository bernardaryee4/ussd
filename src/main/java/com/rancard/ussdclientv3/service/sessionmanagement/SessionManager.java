package com.rancard.ussdclientv3.service.sessionmanagement;

import com.rancard.ussdclientv3.model.Session;
import com.rancard.ussdclientv3.model.dao.SessionDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SessionManager {

    private SessionDao sessionDao;

    private Logger logger  = LoggerFactory.getLogger(SessionManager.class);

    public SessionManager(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    public Session getSessionById(String id){
        Optional<Session> session = sessionDao.findById(id);
        return session.orElse(null);
    }

    public Session persistSession(Session session, String sessionId){
        logger.info("[" + sessionId + "] about to save new session details");

        if(session != null){
            session = sessionDao.save(session);
            logger.info("["+sessionId+"] done saving session details");
            return session;
        }
        logger.info("["+sessionId+"] cannot save empty session details");
        return null;
    }

    public void deleteSession(String ussdSessionId, String sessionId){
        logger.info("[" + sessionId + "] --- about to delete existing session details");

        if(ussdSessionId != null){
            sessionDao.deleteById(ussdSessionId);
            logger.info("["+sessionId+"] done deleting session details");
        }
    }
}
