package com.rancard.ussdclientv3.service.request;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.rancard.ussdclientv3.model.Agent;
import com.rancard.ussdclientv3.model.accountmanagement.AccountManagementRequest;
import com.rancard.ussdclientv3.model.ApiResponse;
import com.rancard.ussdclientv3.model.checkaccounts.CheckAccountRequest;
import com.rancard.ussdclientv3.model.deposit.Deposit;
import com.rancard.ussdclientv3.model.registration.Registration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@Service
public class UssdClientService {

    private Logger logger = LoggerFactory.getLogger(UssdClientService.class);

    @Value("${nthc.agent.url}")
    protected String requestUrl;

    @Value("${backend.user.exists}")
    protected String USER_EXISTS;

    @Value ("${host.endpoint}")
    protected String host;

    public ApiResponse checkAgentExistence(AccountManagementRequest accountManagementRequest, String sessionId){

        logger.info("["+sessionId+"] about to make request to check for agent existence");
        ApiResponse apiResponse = new ApiResponse();

        if(accountManagementRequest != null){
            RestTemplate restTemplate = new RestTemplate();
            //todo --- Make configuration property for this
            String url = host+"api/v1/auth?phone="+accountManagementRequest.getMsisdn() ;
            logger.info("["+sessionId+"] agent existence request url" + url);
            apiResponse = makeGetRequest(sessionId, apiResponse, restTemplate, url);
            logger.info("["+sessionId+"] done making agent existence request");
            logger.info("["+sessionId+"] response from request :" + apiResponse.toString());
        }
        return apiResponse;
    }


    public ApiResponse checkPinStatus(AccountManagementRequest accountManagementRequest, String sessionId){

        ApiResponse apiResponse = new ApiResponse();

        if(accountManagementRequest != null){
            RestTemplate restTemplate = new RestTemplate();
            //todo --- Make configuration property for this
            String url = host+"agent/pin/check?agentPhone="+accountManagementRequest.getMsisdn() ;

            apiResponse = makeGetRequest(sessionId, apiResponse, restTemplate, url);
        }
        return apiResponse;
    }

    public ApiResponse validateAgentPin(AccountManagementRequest accountManagementRequest, String sessionId) {

        ResponseEntity<String> apiResponse = null;
        ApiResponse response = new ApiResponse();

        if(accountManagementRequest != null){
            RestTemplate restTemplate = new RestTemplate();
            //todo --- Make configuration property for this
            String url = host+"api/v1/auth/personnel/pin";
           HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            logger.info("["+sessionId+"] about to make call to : "+ url);

            HttpEntity<String> request =
                    new HttpEntity<>(accountManagementRequest.toValidatePinRequestParams().toString(), headers);
            logger.info("["+sessionId+"] about to make call to : "+ url);


            try {

                logger.info("["+sessionId+"] auth request params "+ request.toString());
                ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(url, request, String.class);

                response = toApiResponse(responseEntityStr);
                
            } catch (HttpClientErrorException e){
                logger.warn("[ "+ sessionId +" ] Error response received code : "+ e.getStatusCode().value()
                                + " : reason : "+ e.getStatusCode().getReasonPhrase() + " : message : "
                        + e.getResponseBodyAsString());
            }

        }
        return response;
    }

    public ApiResponse validateAgentPassword(AccountManagementRequest accountManagementRequest, String sessionId){
        ResponseEntity<String> apiResponse = null;
        ApiResponse response = new ApiResponse();

        if(accountManagementRequest != null){
            RestTemplate restTemplate = new RestTemplate();
            //todo --- Make configuration property for this
            String url = host+"api/v1/auth/personnel";

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            logger.info("["+sessionId+"] about to make call to : "+ url);

            HttpEntity<String> request =
                    new HttpEntity<>(accountManagementRequest.toValidatePasswordRequestParams().toString(), headers);
            try {
                logger.info("["+sessionId+"] auth request params "+ request.toString());
                ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(url, request, String.class);

                response = toApiResponse(responseEntityStr);

            } catch (HttpClientErrorException e){
                logger.warn("[ "+ sessionId +" ] Error response received code : "+ e.getStatusCode().value()
                        + " : reason : "+ e.getStatusCode().getReasonPhrase() + " : message : "
                        + e.getResponseBodyAsString());
            }

            int responseStatus = (apiResponse != null) ? apiResponse.getStatusCodeValue() : 0;

            logger.info("[ "+ sessionId +" ] validate password for agent : "+ responseStatus
                    + " reason : "+ ((apiResponse != null) ? apiResponse.getStatusCode().getReasonPhrase() : "None"));

            if(apiResponse != null) {
                if (apiResponse.getStatusCode().is2xxSuccessful()) {
                    response = toApiResponse(apiResponse);
                    logger.info(String.format("[ " + sessionId + " ] Response: validate password(" + url + ")  : %s", apiResponse));
                }
            }

        }
        logger.info("About to return response "+ response.toString()+" for password authentication");
        return response;
    }


    public ApiResponse setAgentPin(AccountManagementRequest accountManagementRequest, String sessionId){
        ApiResponse response = new ApiResponse();
        ResponseEntity<String> apiResponse = null;

        if(accountManagementRequest != null){
            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

            //todo --- Make configuration property for this
            String url = host+"api/v1/auth/personnel/pin/"+accountManagementRequest.getMsisdn();

            logger.info("["+sessionId+"] about to make call to : "+ url);

            JsonObject requestJson = accountManagementRequest.toSetPinRequestParams();
            HttpEntity<AccountManagementRequest> request = new HttpEntity<>(accountManagementRequest,headers);

            System.out.println(request);
            try {
                apiResponse = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            } catch (HttpClientErrorException e){
                logger.warn("[ "+ sessionId +" ] Error response received code : "+ e.getStatusCode().value()
                        + " : reason : "+ e.getStatusCode().getReasonPhrase() + " : message : "
                        + e.getResponseBodyAsString());
            }

            int responseStatus = (apiResponse != null) ? apiResponse.getStatusCodeValue() : 0;

            logger.info("[ "+ sessionId +" ] set pin for agent : "+ responseStatus
                    + " reason : "+ ((apiResponse != null) ? apiResponse.getStatusCode().getReasonPhrase() : "None"));

            if(apiResponse != null) {
                if (apiResponse.getStatusCode().is2xxSuccessful()) {
                    response = toApiResponse(apiResponse);
                    logger.info(String.format("[ " + sessionId + " ] Response: validate password(" + url + ")  : %s", apiResponse));
                }
            }

        }

        return response;
    }

    private ApiResponse toApiResponse(ResponseEntity<String> response){
        logger.info("About to format response :"+ response.getBody() +" to API response");

        ApiResponse apiResponse = new ApiResponse();
        ObjectMapper mapper = new ObjectMapper();

        try{

            JsonNode root = mapper.readTree(response.getBody());
            apiResponse.setCode(root.path("code") != null ? root.path("code").asInt():0);
            apiResponse.setMessage(root.path("message") != null ? root.path("message").asText():"");
            apiResponse.setTotal(root.path("total") != null ? root.path("total").asDouble():0d);
            apiResponse.setData(root.path("data"));
        }catch (Exception e){
            e.printStackTrace();
            apiResponse.setCode(0);
            apiResponse.setData(null);
            apiResponse.setMessage("exception occurred while processing api response");
        }
        return apiResponse;
    }

    private HttpHeaders getRequestHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        return headers;
    }


    private ApiResponse makeGetRequest(String sessionId, ApiResponse apiResponse, RestTemplate restTemplate, String url) {
        logger.info("["+sessionId+"] about to make call to : "+ url);

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        logger.info("["+sessionId+"] done making HTTP call");
        if(response.getStatusCode().is2xxSuccessful()){
            logger.info("["+sessionId+"] HTTP code response success");
            apiResponse = toApiResponse(response);
            logger.info("["+sessionId+"] response from backend :" + response.getBody());
        }
        return apiResponse;
    }

    public ApiResponse registerClient(Registration registrationDetails, String sessionId){
        logger.info("Registration details received : "+ registrationDetails.toString());

        ResponseEntity<String> apiResponse = null;
        ApiResponse response = new ApiResponse();

        RestTemplate restTemplate = new RestTemplate();
        //todo --- Make configuration property for this
        String url = host+"api/v1/register";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        logger.info("["+sessionId+"] about to make call to : "+ url);

        HttpEntity<String> request =
                new HttpEntity<>(registrationDetails.toDoRegistrationParams().toString(), headers);
        logger.info("["+sessionId+"] about to make call to : "+ url);


        try {

            logger.info("["+sessionId+"] auth request params "+ request.toString());
            ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(url, request, String.class);

            response = toApiResponse(responseEntityStr);

        } catch (HttpClientErrorException e){
            logger.warn("[ "+ sessionId +" ] Error response received code : "+ e.getStatusCode().value()
                    + " : reason : "+ e.getStatusCode().getReasonPhrase() + " : message : "
                    + e.getResponseBodyAsString());
        }

        return response;
    }

    public ApiResponse checkAccount(CheckAccountRequest checkAccountRequest, String sessionId){
        logger.info("[ " + sessionId + " ] GET: calling backend  "
                + (StringUtils.isNotBlank(host) ? host : "None"));

        long start = new Date().getTime();

        if (StringUtils.isNotBlank(host)){

            logger.info("Request params ::: "+checkAccountRequest.toCheckAccountRequest());

            String requestEndpoint = host+"api/v1/deposit/checkAccount?"+checkAccountRequest.toCheckAccountRequest();

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);

            logger.info("[ "+sessionId+" ] about to call nthc with params: " + checkAccountRequest.toCheckAccountRequest());

            logger.info("About to make call to  : "+ requestEndpoint);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> apiResponse = restTemplate
                    .exchange(requestEndpoint, HttpMethod.GET, httpEntity, String.class);

            logger.info("[ "+sessionId+" ] response code from nthc: " + apiResponse.getStatusCodeValue());

            if (apiResponse.getStatusCodeValue() == 200){

                boolean isResponseReturned = (apiResponse.getBody() != null);

                logger.info("[ "+sessionId+" ] Response from backend in "
                        + (new Date().getTime() - start) + "mil seconds"
                        + "response from: backend is: " + (isResponseReturned ? apiResponse.getBody() : " None"));

                if (isResponseReturned){
                    ApiResponse response = toApiResponse(apiResponse);

                    logger.info("[ "+sessionId+" ] GET response from backend: " + response);
                    logger.info(response.toString());
                    return response;
                }
            }
        }

        return null;
    }


    public ApiResponse makeDepositForClient(Deposit depositDetails, String sessionId){
        logger.info("Deposit details received : "+ depositDetails.toString());

        ResponseEntity<String> apiResponse = null;
        ApiResponse response = new ApiResponse();

        RestTemplate restTemplate = new RestTemplate();
        //todo --- Make configuration property for this
        String requestEndpoint = host+"api/v1/deposit";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        logger.info("["+sessionId+"] about to make call to : "+ requestEndpoint);

        HttpEntity<String> request =
                new HttpEntity<>(depositDetails.toDoDepositParams().toString(), headers);
        logger.info("["+sessionId+"] about to make call to : "+ requestEndpoint);


        try {

            logger.info("["+sessionId+"] deposit request params "+ request.toString());
            ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(requestEndpoint, request, String.class);

            response = toApiResponse(responseEntityStr);

        } catch (HttpClientErrorException e){
            logger.warn("[ "+ sessionId +" ] Error response received code : "+ e.getStatusCode().value()
                    + " : reason : "+ e.getStatusCode().getReasonPhrase() + " : message : "
                    + e.getResponseBodyAsString());
        }

        return response;
    }


    public ApiResponse checkCollection(Agent agent, String sessionId){
        logger.info("[ " + sessionId + " ] GET: calling backend  "
                + (StringUtils.isNotBlank(host) ? host : "None"));

        long start = new Date().getTime();

        if (StringUtils.isNotBlank(host)){

            logger.info("Request params ::: "+agent.toString());

            String requestEndpoint = host+"/api/v1/collections?personnelId="+agent.getId();

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);

            logger.info("[ "+sessionId+" ] about to call nthc with params: " + agent.getId());

            logger.info("About to make call to  : "+ requestEndpoint);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> apiResponse = restTemplate
                    .exchange(requestEndpoint, HttpMethod.GET, httpEntity, String.class);

            logger.info("[ "+sessionId+" ] response code from backend: " + apiResponse.getStatusCodeValue());

            if (apiResponse.getStatusCodeValue() == 200){

                boolean isResponseReturned = (apiResponse.getBody() != null);

                logger.info("[ "+sessionId+" ] Response from backend in "
                        + (new Date().getTime() - start) + "mil seconds"
                        + "response from: backend is: " + (isResponseReturned ? apiResponse.getBody() : " None"));

                if (isResponseReturned){
                    ApiResponse response = toApiResponse(apiResponse);

                    logger.info("[ "+sessionId+" ] GET response from backend: " + response);
                    logger.info(response.toString());
                    return response;
                }
            }
        }

        return null;
    }
}
