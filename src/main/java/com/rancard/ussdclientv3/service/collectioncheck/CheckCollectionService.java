package com.rancard.ussdclientv3.service.collectioncheck;

import com.fasterxml.jackson.databind.JsonNode;
import com.rancard.ussdclientv3.controller.UssdRequest;
import com.rancard.ussdclientv3.controller.UssdResponse;
import com.rancard.ussdclientv3.model.*;

import com.rancard.ussdclientv3.service.request.UssdClientService;
import com.rancard.ussdclientv3.service.sessionmanagement.SessionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CheckCollectionService {


    private Logger logger = LoggerFactory.getLogger(CheckCollectionService.class);
    private UssdClientService ussdClientService;
    private SessionManager sessionManager;


    public CheckCollectionService(UssdClientService ussdClientService,SessionManager sessionManager) {
        this.ussdClientService = ussdClientService;
    }

    public UssdResponse checkCollection(UssdRequest ussdRequest, String sessionId, Session session, UssdResponse
            ussdResponse) {

        Agent agent = session.getAgentProperties();


        if(agent == null){
            agent = new Agent();
        }

        logger.info("[" + sessionId + "] about to process client request to check collection " + session.getMenuLevel());
        ApiResponse apiResponse = ussdClientService.checkCollection(agent, sessionId);

        logger.info("["+sessionId+"] response received from backend : "+apiResponse.toString());
        if(apiResponse.getCode() == 200){
            if(!apiResponse.getData().isNull()){
                ussdResponse.setMessage(stringifyCollectionsList(apiResponse));
            }else{
                ussdResponse.setMessage("An error occurred while checking collections");
            }
        }else{
                ussdResponse.setMessage("You have made no collections today");
        }
        ussdResponse.setContinueSession(false);

        return ussdResponse;
    }

    private String stringifyCollectionsList(ApiResponse apiResponse){

        JsonNode agentCollections = apiResponse.getData();
        logger.info("Agent collections : "+ agentCollections.toString());
        StringBuilder collections = new StringBuilder();

        int counter = 1;

        for (JsonNode collection : agentCollections) {
            collections.append(counter).append(". ").append(collection.get("accountNumber").asText()).append(" - GHS")
                    .append(collection.get("amount").asText()).append("\n");
            counter++;
        }

        logger.info("Collections to string : " + collections);
        return "Your collections for today : \n"+collections.toString()+"\nTotal collections today : GHS"+ apiResponse.getTotal();
    }

}
