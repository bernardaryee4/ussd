package com.rancard.ussdclientv3.service.paymentmethod;

import com.rancard.ussdclientv3.controller.UssdRequest;
import com.rancard.ussdclientv3.controller.UssdResponse;
import com.rancard.ussdclientv3.model.Client;
import com.rancard.ussdclientv3.model.MENU_OPTION;
import com.rancard.ussdclientv3.model.PRODUCT;
import com.rancard.ussdclientv3.model.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PaymentMethodService {

    @Value("${response.registration.confirmation}")
    protected String REG_CONFIRM;

    @Value("${response.deposit.confirmation}")
    protected String DEP_CONFIRM;

    @Value("${response.request.valid.input}")
    protected String REQUEST_VALID_INPUT;

    @Value("${response.back}")
    protected String BACK;

    @Value("${response.payment.method}")
    protected String PAYMENT_METHOD;





    private Logger logger = LoggerFactory.getLogger(PaymentMethodService.class);
    public UssdResponse processCashPayment(Session session, String sessionId, UssdResponse ussdResponse,
                                           UssdRequest ussdRequest){
        logger.info("["+sessionId+"] processing cash payment request");
        Client client = session.getClientProperties();
        if(client == null){
            logger.info("["+sessionId+"] called process Cash Payment with empty client. Returning null");
            return null;
        }
        double cashAmount = Double.parseDouble(ussdRequest.getMessage());
        boolean metMinimumAmount = false;
        String confirmationMessage ="";



        logger.info("["+sessionId+"] request to process cash payment for amount: "+ussdRequest.getMessage());

        if(session.getMenuLevel().equals("cashOnDeposit")){
            metMinimumAmount = true;
        }else {
            String initialDeposit = PRODUCT.initialDeposit(client.getProduct());

            if(initialDeposit != null){
                metMinimumAmount = cashAmount >  Double.parseDouble(initialDeposit);
            }
        }

        if(MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
            logger.info("["+sessionId+"] User chose to go to previous menu from cash initial deposit sub menu");
            ussdResponse.setMessage(PAYMENT_METHOD+BACK);
            if(session.getMenuLevel().equals("cashOnDeposit")){
                session.setMenuLevel("depositPaymentMethod");
            }else{
                session.setMenuLevel("paymentMethod");
            }
            ussdResponse.setContinueSession(true);
            return ussdResponse;
        }else if(metMinimumAmount){
            logger.info("["+sessionId+"] User met requirements for cash deposit on "+ client.getProduct());
            client.setAmount(ussdRequest.getMessage());
            if(session.getMenuLevel().equals("cashOnDeposit")){
                ussdResponse.setMessage(String.format(DEP_CONFIRM, client.getProduct().toString(), client.getDepositIdNumber(),
                        client.getAmount(), client.getFirstName()));
                session.setMenuLevel("confirmDeposit");

            }else{
                ussdResponse.setMessage(String.format(REG_CONFIRM, client.getProduct().toString(), client.getFirstName(),
                        client.getLastName(), client.getAmount()));
                session.setMenuLevel("confirmRegistration");
            }
            ussdResponse.setContinueSession(true);
            logger.info("["+sessionId+"] Session State :"+ session.toString());
            return ussdResponse;
        }else{
            ussdResponse.setMessage(REQUEST_VALID_INPUT+BACK);
            session.setMenuLevel("cash");
            ussdResponse.setContinueSession(true);
            return ussdResponse;
        }
    }

    public UssdResponse processMomoPayment(Session session, String sessionId, UssdResponse ussdResponse){
        return null;
    }

    public UssdResponse processChequePayent(Session session, String sessionId, UssdResponse ussdResponse){
        return null;
    }
}
