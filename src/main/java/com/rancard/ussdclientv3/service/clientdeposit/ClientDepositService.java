package com.rancard.ussdclientv3.service.clientdeposit;

import com.fasterxml.jackson.databind.JsonNode;
import com.rancard.ussdclientv3.controller.UssdRequest;
import com.rancard.ussdclientv3.controller.UssdResponse;
import com.rancard.ussdclientv3.model.*;
import com.rancard.ussdclientv3.model.checkaccounts.CheckAccountRequest;
import com.rancard.ussdclientv3.model.deposit.Deposit;
import com.rancard.ussdclientv3.service.paymentmethod.PaymentMethodService;
import com.rancard.ussdclientv3.service.request.UssdClientService;
import com.rancard.ussdclientv3.service.sessionmanagement.SessionManager;
import com.rancard.ussdclientv3.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;

import java.util.Map;

@Service
public class ClientDepositService {

    @Value("${response.deposit.select.product}")
    protected String DEPOSIT_SELECT_PRODUCT;

    @Value("${response.welcome.message}")
    protected String WELCOME_MESSAGE;

    @Value("${response.back}")
    protected String BACK;

    @Value("${response.account.id}")
    protected String GET_ACCOUNT_ID;

    @Value("${response.request.valid.option}")
    protected String REQUEST_VALID_OPTION;

    @Value("${response.request.valid.input}")
    protected String REQUEST_VALID_INPUT;

    @Value("${response.product.selection}")
    protected String SELECT_PRODUCT;

    @Value("${response.payment.method}")
    protected String PAYMENT_METHOD;

    @Value("${response.deposit.success}")
    protected String DEPOSIT_SUCCESS;

    @Value("${response.another.transaction}")
    protected String ANOTHER_TRANSACTION;

    @Value("${response.thank.you}")
    protected String THANK_YOU;


    private Logger logger = LoggerFactory.getLogger(ClientDepositService.class);

    private UssdClientService ussdClientService;
    private PaymentMethodService paymentMethodService;
    private SessionManager sessionManager;

    public ClientDepositService(UssdClientService ussdClientService, PaymentMethodService paymentMethodService,SessionManager sessionManger) {
        this.ussdClientService = ussdClientService;
        this.paymentMethodService = paymentMethodService;
        this.sessionManager = sessionManger;
    }

    public UssdResponse makeDeposit(UssdRequest ussdRequest, String sessionId, Session session, UssdResponse ussdResponse) {

        Client client = session.getClientProperties();
        Agent agent = session.getAgentProperties();

        if(client == null){
            client = new Client();
        }

        if(agent == null){
            agent = new Agent();
        }

        logger.info("[" + sessionId + "] about to process client deposit request @ menu " + session.getMenuLevel());
        switch (session.getMenuLevel()) {

            case "depositProductSelect":
                logger.info("["+sessionId+"] user selected product option "+ussdRequest.getMessage());
                if(MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    logger.info("["+sessionId+"] user chose to go to previous menu item. from deposit " +
                            "product selection sub menu");
                    ussdResponse.setMessage(WELCOME_MESSAGE);
                    session.setMenuLevel("agentDefaultMenu");
                    session.setSubMenuLevel(null);
                }else{
                    PRODUCT selectedProduct = PRODUCT.fromChoice(ussdRequest.getMessage().trim());
                    if(selectedProduct != null){
                        client.setProduct(selectedProduct);
                        ussdResponse.setMessage(SELECT_PRODUCT+BACK);
                        session.setMenuLevel("depositGetAccountId");
                        session.setClientProperties(client);
                    }else{
                        logger.info("["+sessionId+"] user selected product invalid product option"
                                +ussdRequest.getMessage());
                        ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+SELECT_PRODUCT+BACK);
                        session.setMenuLevel("depositProductSelect");

                    }
                }
                ussdResponse.setContinueSession(true);
                break;
            case "depositGetAccountId":
                if (MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())) {
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(SELECT_PRODUCT+BACK);
                    session.setMenuLevel("depositProductSelect");
                } else if (session.getClientProperties().getProduct() != null) {
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(GET_ACCOUNT_ID+BACK);
                    session.setMenuLevel("selectAccount");
                } else {
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(DEPOSIT_SELECT_PRODUCT);
                    session.setMenuLevel("depositUserId");
                }
                break;
            case "selectAccount":
                if (MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())) {
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(SELECT_PRODUCT+BACK);
                    session.setMenuLevel("depositProductSelect");
                } else {
                    Map<String, String> identifier = Utils.checkAndReturnIdentification(
                            StringUtils.trim(ussdRequest.getMessage()));

                    boolean isValidID = Boolean.parseBoolean(identifier.get("isValid").toString());
                    if (isValidID) {
                        client.setDepositId(DEPOSIT_ID.fromChoice(identifier.get("clientIdType")));
                        client.setDepositIdNumber(identifier.get("clientIdNo"));
                        CheckAccountRequest checkAccountRequest = new CheckAccountRequest(client);
                        ApiResponse apiResponse = ussdClientService.checkAccount(checkAccountRequest,sessionId);

                        logger.info("Response received : " + apiResponse.toString());
                        if(apiResponse.getCode() == 200){
                            if(!apiResponse.getData().isNull()){
                                ussdResponse.setMessage(stringifyAccountsList(apiResponse.getData(),client));
                                ussdResponse.setContinueSession(true);
                                session.setMenuLevel("selectAccountToCredit");
                            }else{
                                ussdResponse.setMessage("Account not found\n"+GET_ACCOUNT_ID+BACK);
                                ussdResponse.setContinueSession(true);
                                session.setMenuLevel("selectAccount");
                            }
                        }
                    }
                }
                break;
            case "selectAccountToCredit":
                if (MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())) {
                    ussdResponse.setContinueSession(true);
                    ussdResponse.setMessage(SELECT_PRODUCT + BACK);
                    session.setMenuLevel("depositProductSelect");
                }else{
                    logger.info("About to get account user selected from map");
                    if(Integer.parseInt(ussdRequest.getMessage()) <= client.getAccountMap().size() && Integer.parseInt(ussdRequest.getMessage()) > 0) {
                        client.setDepositIdNumber(client.getAccountMap().get(Integer.parseInt(ussdRequest.getMessage())));
                        ussdResponse.setMessage(PAYMENT_METHOD + BACK);
                        ussdResponse.setContinueSession(true);
                        session.setMenuLevel("depositPaymentMethod");
                    }else{
                        ussdResponse.setMessage(client.getAccounts());
                        ussdResponse.setContinueSession(true);
                        session.setMenuLevel("selectAccountToCredit");
                    }
                }
                break;
            case "depositPaymentMethod":
                PAYMENT_MEDIUM paymentMedium = PAYMENT_MEDIUM.fromChoice(ussdRequest.getMessage());
                boolean isValidPaymentMethodRequest = paymentMedium != null;
                if(MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    logger.info("["+sessionId+"] User chose to go to previous menu from paymentMethod sub menu");
                    ussdResponse.setMessage(GET_ACCOUNT_ID+BACK);
                    session.setMenuLevel("selectAccountToCredit");
                    ussdResponse.setContinueSession(true);
                }else if(isValidPaymentMethodRequest){
                    logger.info("["+sessionId+"] set client payment method to: "+paymentMedium.toString());
                    client.setPaymentMedium(paymentMedium);
                    if(paymentMedium == PAYMENT_MEDIUM.CASH){
                        ussdResponse.setMessage("Enter amount"+BACK);
                        ussdResponse.setContinueSession(true);
                        session.setMenuLevel("cashOnDeposit");
                    }else if(paymentMedium == PAYMENT_MEDIUM.MOBILE_MONEY){
                        ussdResponse.setMessage("Mobile Money option not available"+BACK);
                        session.setMenuLevel("mobileMoneyOnDeposit");
                        ussdResponse.setContinueSession(false);
                    }else{
                        ussdResponse.setMessage("Cheque option not available"+BACK);
                        session.setMenuLevel("chequeOnDeposit");
                        ussdResponse.setContinueSession(false);
                    }
                    session.setClientProperties(client);
                }else{
                    logger.info("["+sessionId+"] user tried to input payment medium "+ussdRequest.getMessage());
                    ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+PAYMENT_METHOD+BACK);
                    ussdResponse.setContinueSession(true);
                    session.setMenuLevel("depositPaymentMethod");
                }
                break;
            case "cashOnDeposit":
                ussdResponse = paymentMethodService.processCashPayment(session, sessionId, ussdResponse, ussdRequest);
                break;
            case "mobileMoneyOnDeposit":
                break;
            case "chequeOnDeposit":
                break;
            case "confirmDeposit":
                logger.info("About to confirm deposit");
                if(MENU_OPTION.ZERO == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    switch (client.getPaymentMedium()){
                        case CASH:
                            logger.info("["+sessionId+"] User chose to go to previous menu from confirm deposit " +
                                    "cash  sub menu");
                            ussdResponse.setMessage("Enter amount "+BACK);
                            session.setMenuLevel("depositPaymentMethod");

                            break;
                        case CHEQUE:
                            break;
                        case MOBILE_MONEY:
                            break;
                    }
                    session.setMenuLevel("cashOnDeposit");
                    ussdResponse.setContinueSession(true);
                }else if(MENU_OPTION.ONE == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    logger.info("["+sessionId+"] User has confirmed deposit of client"+client.toString());
                    Deposit depositDetails = new Deposit(client);
                    depositDetails.setAgentId(agent.getId());
                    depositDetails.setAgentPhone(agent.getMsisdn());

                    ApiResponse apiResponse = ussdClientService.makeDepositForClient(depositDetails, sessionId);

                    if(apiResponse != null){
                        logger.info("["+sessionId+"] Response received : "+ apiResponse.toString());

                        if(apiResponse.getCode() == 200){
                            String accountNumber = apiResponse.getData().get(0).get("accountNumber").asText();
                            String transactionId = apiResponse.getData().get(0).get("transactionId").asText();
                            ussdResponse.setContinueSession(true);
                            ussdResponse.setMessage(String.format(DEPOSIT_SUCCESS,accountNumber,transactionId,ANOTHER_TRANSACTION));
                            String ussdSessionId = ussdRequest.getSessionId() + "@" + ussdRequest.getMobileNetwork();
                            session.setMenuLevel("depositSuccess");
                        }else{
                            ussdResponse.setContinueSession(false);
                            ussdResponse.setMessage(apiResponse.getMessage()+"\n Try again later");
                            String ussdSessionId = ussdRequest.getSessionId() + "@" + ussdRequest.getMobileNetwork();
                            sessionManager.deleteSession(ussdSessionId,sessionId);
                        }
                    }

                }else{
                    logger.info("["+sessionId+"] user tried to confirm with wrong input "+ussdRequest.getMessage());
                    ussdResponse.setMessage(REQUEST_VALID_OPTION+"\n"+PAYMENT_METHOD+BACK);
                    ussdResponse.setContinueSession(true);
                    session.setMenuLevel("confirmDeposit");
                }
                break;
            case "depositSuccess":
                if(MENU_OPTION.ONE == MENU_OPTION.fromChoice(ussdRequest.getMessage())){
                    logger.info("["+sessionId+"] user chose to go to home");
                    ussdResponse.setMessage(WELCOME_MESSAGE);
                    session.setMenuLevel("agentDefaultMenu");
                    session.setSubMenuLevel(null);
                    ussdResponse.setContinueSession(true);
                    }else{
                    logger.info("["+sessionId+"] user chose to exit");
                    ussdResponse.setMessage(THANK_YOU);
                    session.setSubMenuLevel(null);
                    ussdResponse.setContinueSession(false);
                    String ussdSessionId = ussdRequest.getSessionId() + "@" + ussdRequest.getMobileNetwork();
                    sessionManager.deleteSession(ussdSessionId,sessionId);

                }

        }

        return ussdResponse;
    }

    private String stringifyAccountsList(JsonNode accounts,Client client){

        StringBuilder depositAccounts = new StringBuilder();

        int counter = 1;

        Map<Integer, String > accountMap = new HashMap<>();
        for (JsonNode account : accounts) {
            accountMap.put(counter,account.get("accNo").asText());
            client.setAccountMap(accountMap);
            client.setFirstName(account.get("accountName").asText());

            depositAccounts.append(counter).append(". ").append(account.get("accNo").asText()).append(" ")
                    .append(account.get("accountName").asText()).append("\n");
            counter++;
        }

        logger.info("Deposit accounts to string : " + depositAccounts);
        client.setAccounts("Select an account\n"+depositAccounts.toString());
        return "Select an account\n"+depositAccounts.toString();
    }

}
