package com.rancard.ussdclientv3.service.accountmanagement;

import com.fasterxml.jackson.databind.JsonNode;
import com.rancard.ussdclientv3.controller.UssdRequest;
import com.rancard.ussdclientv3.controller.UssdResponse;
import com.rancard.ussdclientv3.model.accountmanagement.AccountManagementRequest;
import com.rancard.ussdclientv3.model.Agent;
import com.rancard.ussdclientv3.model.ApiResponse;
import com.rancard.ussdclientv3.model.Session;
import com.rancard.ussdclientv3.service.request.UssdClientService;
import com.rancard.ussdclientv3.service.sessionmanagement.SessionManager;
import com.rancard.ussdclientv3.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AccountManagementService {

    private Logger logger = LoggerFactory.getLogger(AccountManagementService.class);

    private UssdClientService ussdClientService;

    private SessionManager sessionManager;

    @Value("${response.welcome.message}")
    protected String WELCOME_MESSAGE;

    @Value("${response.invalid.pin}")
    protected String INVALID_PIN;

    @Value("${response.invalid.msisdn}")
    protected String INVALID_MSISDN;

    @Value("${response.set.pin}")
    protected String ENTER_PIN;

    @Value("${response.set.pin.success}")
    protected String PIN_SET_SUCCESS;

    @Value("${response.password.mismatch}")
    protected String PASSWORD_MISMATCH;

    @Value("${response.confirm.pin}")
    protected String CONFIRM_PIN;

    @Value("${response.mismatch.pin}")
    protected String PIN_MISMATCH;



    public AccountManagementService(UssdClientService ussdClientService, SessionManager sessionManager){
        this.ussdClientService = ussdClientService;
        this.sessionManager = sessionManager;
    }



    public UssdResponse checkAgentExistence(UssdRequest ussdRequest, String sessionId, Session session,UssdResponse response){

        logger.info("["+sessionId+"] about to check agent existence with request params "+ ussdRequest.toString());

        ApiResponse apiResponse = new ApiResponse();

        AccountManagementRequest accountManagementRequest = new AccountManagementRequest();
        accountManagementRequest.setMsisdn(ussdRequest.getMsisdn());

        String msisdn = (accountManagementRequest.getMsisdn() != null ? accountManagementRequest.getMsisdn() :"");

        if( Utils.isValidMsisdn(msisdn)){
            logger.info("["+sessionId+"] msisdn validated");
            logger.info("["+sessionId+"] about to make agent existence call");
            apiResponse =  ussdClientService.checkAgentExistence(accountManagementRequest,sessionId);

            if (apiResponse != null) {
                if (apiResponse.getCode() == 200) {
                    session.setMenuLevel("checkPin");

                    if (apiResponse.getData().get(0).get("pin").isNull()) {
                        response.setMessage("Enter your password to set a PIN");
                        session.setMenuLevel("enterPassword");
                        } else {
                        response.setMessage("Enter your PIN");
                        session.setMenuLevel("enterPin");
                        }

                } else {
                    response.setMessage("User menus are currently unavailable\nPlease try again later");
                    response.setContinueSession(false);
                    String ussdSessionId = ussdRequest.getSessionId() + "@" + ussdRequest.getMobileNetwork();
                    sessionManager.deleteSession(ussdSessionId,sessionId);
                }
            }

        } else {
            logger.info("["+sessionId+"] msisdn invalid");
            response.setMessage(INVALID_MSISDN);
            response.setContinueSession(false);
        }
        return response;
    }


    public UssdResponse validateAgentPin(UssdRequest ussdRequest, String sessionId,Session session,UssdResponse response){

        AccountManagementRequest accountManagementRequest = new AccountManagementRequest();
        accountManagementRequest.setPin(
                ussdRequest.getMessage() != null ? ussdRequest.getMessage() : "");
        accountManagementRequest.setMsisdn(
                ussdRequest.getMsisdn() != null ? ussdRequest.getMsisdn() : "");

        logger.info("["+sessionId+"] about to validate agent pin with request :: "+ accountManagementRequest.toString());

        ApiResponse apiResponse = new ApiResponse();

        String msisdn = accountManagementRequest.getMsisdn();
        String pin = accountManagementRequest.getPin();

        if(pin != null){
            logger.info("["+sessionId+"] about to call api for pin validation");
            apiResponse  = ussdClientService.validateAgentPin(accountManagementRequest,sessionId);
        }

        if(apiResponse != null){
            if(apiResponse.getCode() == 200){
                response.setMessage(WELCOME_MESSAGE);
                assignAgent(apiResponse.getData(), session);
                response.setContinueSession(true);
                session.setMenuLevel("agentDefaultMenu");
            }else{
                response.setMessage(INVALID_PIN);
                response.setContinueSession(true);
                session.setMenuLevel("enterPin");
            }
        }
        return  response;
    }

    public UssdResponse validateAgentPassword(UssdRequest ussdRequest, String sessionId,Session session, UssdResponse response){

        AccountManagementRequest accountManagementRequest = new AccountManagementRequest();
        accountManagementRequest.setMsisdn(ussdRequest.getMsisdn() != null ? ussdRequest.getMsisdn() : "");
        accountManagementRequest.setPassword(ussdRequest.getMessage() != null ? ussdRequest.getMessage() : "");

        logger.info("["+sessionId+"] about to validate agent password with request :: "+ accountManagementRequest.toString());
        ApiResponse apiResponse = new ApiResponse();
        String msisdn = accountManagementRequest.getMsisdn();
        String password = accountManagementRequest.getPassword();

        if(password != null && msisdn != null){
            logger.info("["+sessionId+"] about to call api for password validation");
            apiResponse = ussdClientService.validateAgentPassword(accountManagementRequest,sessionId);
        }

        if(apiResponse != null){
            if(apiResponse.getCode() == 200){
                response.setMessage(ENTER_PIN);
                response.setContinueSession(true);
                session.setMenuLevel("setPin");
            }else{
                response.setMessage(PASSWORD_MISMATCH);
                response.setContinueSession(true);
                session.setMenuLevel("enterPassword");
            }
        }

        return response;
    }

    public UssdResponse setAgentPin(UssdRequest ussdRequest, String sessionId, Session session, UssdResponse response){
        logger.info("["+sessionId+"] about to set agent pin with request :: "+ ussdRequest.toString());

        ApiResponse apiResponse = new ApiResponse();

        boolean isValidPin = confirmPin(ussdRequest,session,sessionId);

        if(isValidPin) {
            AccountManagementRequest accountManagementRequest = new AccountManagementRequest();
            accountManagementRequest.setMsisdn(ussdRequest.getMsisdn());
            accountManagementRequest.setPin(ussdRequest.getMessage());

            String msisdn = accountManagementRequest.getMsisdn();
            String pin = accountManagementRequest.getPin();

            if (pin != null && msisdn != null) {
                logger.info("[" + sessionId + "] about to call api for pin set");
                apiResponse = ussdClientService.setAgentPin(accountManagementRequest, sessionId);
            }
        }

        if(apiResponse != null){
            response.setMessage(PIN_SET_SUCCESS);
            response.setContinueSession(false);
            String ussdSessionId = ussdRequest.getSessionId() + "@" + ussdRequest.getMobileNetwork();
            sessionManager.deleteSession(ussdSessionId,sessionId);

        }else {
            response.setMessage(PIN_MISMATCH);
            response.setContinueSession(true);
            session.setMenuLevel("repeatPin");
        }
        return response;
    }


    public UssdResponse checkPin(UssdRequest ussdRequest,String sessionId,Session session, UssdResponse ussdResponse) {

        Agent agent = new Agent();
        agent.setPin(ussdRequest.getMessage());
        agent.setId(ussdRequest.getSessionId());
        session.setAgentProperties(agent);

        String pin = ussdRequest.getMessage();
        boolean isValidPin = false;

        try {
            Integer.parseInt(pin);
            isValidPin = pin.length() == 4;

        } catch (NumberFormatException e) {
            logger.info("["+sessionId+"] pin validation error");
        }

        if(isValidPin){
            ussdResponse.setMessage(CONFIRM_PIN);
            ussdResponse.setContinueSession(true);
            session.setMenuLevel("repeatPin");
        }else{
            ussdResponse.setMessage(INVALID_PIN);
            ussdResponse.setContinueSession(true);
            session.setMenuLevel("setPin");
        }
        return ussdResponse;
    }

    private boolean confirmPin(UssdRequest ussdRequest, Session session, String sessionId){

        logger.info("["+sessionId+"] about to validate PIN sent by msisdn:"+ussdRequest.getMsisdn());

        String firstPin = session.getAgentProperties().getPin();
        String secondPin = ussdRequest.getMessage();

        return firstPin.equals(secondPin);

    }

    private Session assignAgent(JsonNode data, Session session) {

        Agent agent = new Agent();
        if(!data.get(0).get("phone").isNull()){
            agent.setMsisdn(data.get(0).get("phone").asText());
        }

        if(!data.get(0).get("personnelId").isNull()){
            agent.setId(data.get(0).get("personnelId").asText());
        }
        session.setAgentProperties(agent);
        return  session;
    }


}
