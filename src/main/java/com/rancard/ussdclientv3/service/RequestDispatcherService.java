package com.rancard.ussdclientv3.service;

import com.rancard.ussdclientv3.controller.UssdRequest;
import com.rancard.ussdclientv3.controller.UssdResponse;
import com.rancard.ussdclientv3.model.MENU_OPTION;
import com.rancard.ussdclientv3.model.Session;
import com.rancard.ussdclientv3.service.accountmanagement.AccountManagementService;
import com.rancard.ussdclientv3.service.clientdeposit.ClientDepositService;
import com.rancard.ussdclientv3.service.clientregistration.RegistrationService;
import com.rancard.ussdclientv3.service.collectioncheck.CheckCollectionService;
import com.rancard.ussdclientv3.service.sessionmanagement.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RequestDispatcherService {

  private AccountManagementService accountManagementService;
  private Logger logger = LoggerFactory.getLogger(RequestDispatcherService.class);
  private SessionManager sessionManager;
  private RegistrationService registrationService;
  private ClientDepositService clientDepositService;
  private CheckCollectionService checkCollectionService;

  @Value("${response.welcome.message}")
  protected String WELCOME_MESSAGE;

  @Value("${response.product.selection}")
  protected String REGISTRATION_DEFAULT_MENU;


  @Value("${response.product.selection}")
  protected String SELECT_PRODUCT;

  @Autowired
  public RequestDispatcherService(AccountManagementService accountManagementService, SessionManager sessionManager, RegistrationService registrationService, ClientDepositService clientDepositService,CheckCollectionService checkCollectionService) {
    this.accountManagementService = accountManagementService;
    this.registrationService = registrationService;
    this.sessionManager = sessionManager;
    this.clientDepositService = clientDepositService;
    this.checkCollectionService = checkCollectionService;
  }

  public UssdResponse processRequest(
      UssdRequest ussdRequest, String menu, Session session, String sessionId) {
        UssdResponse ussdResponse = new UssdResponse("Error processing request",true);

    logger.info(
        "["
            + sessionId
            + "] about to process request for "
            + ((ussdRequest.getMsisdn() != null) ? ussdRequest.getMsisdn() : "None"));
    menu = (session.getMenuLevel() != null ? session.getMenuLevel() : menu);
    String subMenuLevel = session.getSubMenuLevel();

    if(subMenuLevel != null) {
        logger.info("["+sessionId+"] found sub menu "+ subMenuLevel + " for agent with msisdn " + ussdRequest.getMsisdn());
        switch (subMenuLevel){
            case "registration":
                ussdResponse = registrationService.registerClient(ussdRequest,sessionId,session,ussdResponse);
                break;
            case "deposit":
                ussdResponse = clientDepositService.makeDeposit(ussdRequest,sessionId,session,ussdResponse);
                break;
            case "changePin":
                break;
            default:
                break;
        }

            return ussdResponse;
    }


    switch (menu) {

      case "enterPin":
        logger.info("[" + session + "] 'enter pin' menu level found");
         ussdResponse = accountManagementService.validateAgentPin(ussdRequest,sessionId,session,ussdResponse);
        break;

        case "enterPassword":
            logger.info("["+sessionId+"] about the validate agent password");
            ussdResponse = accountManagementService.validateAgentPassword(ussdRequest,sessionId,session,ussdResponse);
            break;
        case "setPin":
            logger.info("["+sessionId+"] about to confirm pin validity for agent");
            ussdResponse = accountManagementService.checkPin(ussdRequest,sessionId,session,ussdResponse);
            break;
        case "repeatPin":
            logger.info("["+sessionId+"] about to set pin for agent");
            ussdResponse = accountManagementService.setAgentPin(ussdRequest,sessionId,session,ussdResponse);
            break;

        case "agentDefaultMenu":
            MENU_OPTION menuOption = MENU_OPTION.fromChoice(ussdRequest.getMessage());
            switch (menuOption){
                case ONE:
                    logger.info("["+sessionId+"] registration menu option selected for agent with msisdn " + ussdRequest.getMsisdn());
                    session.setSubMenuLevel("registration");
                    session.setMenuLevel("productSelection");
                    ussdResponse.setMessage(REGISTRATION_DEFAULT_MENU);
                    ussdResponse.setContinueSession(true);
                    break;
                case TWO:
                    session.setSubMenuLevel("deposit");
                    session.setMenuLevel("depositProductSelect");
                    ussdResponse = clientDepositService.makeDeposit(ussdRequest,sessionId,session,ussdResponse);
                    break;
                case THREE:
                    session.setSubMenuLevel("checkCollection");
                    ussdResponse = checkCollectionService.checkCollection(ussdRequest,sessionId,session,ussdResponse);
                    break;
                case FOUR:
                    //todo --- Change PIN
                    session.setSubMenuLevel("changePin");
                    session.setMenuLevel("changePinAgentPassword");
                    break;
                case ZERO:
                    //todo --- Exit
                    break;
                default:
                    break;
            }
    }

    logger.info("Session after setting agent :  "+ session);
    sessionManager.persistSession(session,sessionId);
    return ussdResponse;
  }
}
