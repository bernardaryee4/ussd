package com.rancard.ussdclientv3.model;

import java.util.Arrays;

public enum ID {
    VOTER("1","VOTERS ID"),
    DRIVER("2","DRIVERS LICENSE"),
    NATIONAL("3","NATIONAL ID"),
    PASSPORT("4","PASSPORT"),
    NHIS("5","NATIONAL HEALTH"),
    CRC("6","COMPANY REGISTRATION CERTIFICATE");


    public String choice;
    public String name;

    ID(String choice,String name){
        this.choice = choice;
        this.name = name;
    }

    public static ID fromChoice(String choice){
        ID idType = null;
        if(choice !=null){
            idType = Arrays.stream(ID.values()).filter(c -> { return choice.equalsIgnoreCase(c.choice); })
                    .findFirst().orElse(null);
        }
        return idType;
    }


}
