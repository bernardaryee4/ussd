package com.rancard.ussdclientv3.model;

import java.util.Arrays;

public enum DEPOSIT_ID {
    PHONE("1","PHONE"),
    EMAIL("2","EMAIL"),
    PRODUCT_ACCT_NUMBER("3","PRODUCT ACCOUNT NUMBER");



    public String choice;
    public String name;

    DEPOSIT_ID(String choice, String name){
        this.choice = choice;
        this.name = name;
    }

    public static DEPOSIT_ID fromChoice(String choice){
        DEPOSIT_ID idType = null;
        if(choice !=null){
            idType = Arrays.stream(DEPOSIT_ID.values()).filter(c -> { return choice.equalsIgnoreCase(c.choice); })
                    .findFirst().orElse(null);
        }
        return idType;
    }


}
