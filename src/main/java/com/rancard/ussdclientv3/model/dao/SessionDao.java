package com.rancard.ussdclientv3.model.dao;

import com.rancard.ussdclientv3.model.Session;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionDao extends CrudRepository<Session,String> {
}
