package com.rancard.ussdclientv3.model.checkaccounts;

import com.rancard.ussdclientv3.model.Client;
import com.rancard.ussdclientv3.model.DEPOSIT_ID;
import com.rancard.ussdclientv3.model.PRODUCT;

public class CheckAccountRequest {
    private String clientIdType;
    private String clientId;
    private String productType;

    public CheckAccountRequest() {
    }

    public CheckAccountRequest(Client client) {
        this.clientIdType = DEPOSIT_ID.valueOf(client.getDepositId().toString()).toString();
        this.clientId = client.getDepositIdNumber();
        this.productType = PRODUCT.valueOf(client.getProduct().toString()).toString();
    }

    public CheckAccountRequest(String clientIdType, String clientId, String productType) {
        this.clientIdType = clientIdType;
        this.clientId = clientId;
        this.productType = productType;
    }

    public String getClientIdType() {
        return clientIdType;
    }

    public void setClientIdType(String clientIdType) {
        this.clientIdType = clientIdType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String toCheckAccountRequest() {
        return "clientId="+this.getClientId()+"&clientIdType="+this.getClientIdType()+"&productType="+this.getProductType();
    }
}
