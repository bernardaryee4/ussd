package com.rancard.ussdclientv3.model;

import java.util.Objects;

public class Agent {

    private String id;
    private String msisdn;
    private String password;
    private String pin;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Agent agent = (Agent) o;
        return Objects.equals(id, agent.id) &&
                Objects.equals(msisdn, agent.msisdn) &&
                Objects.equals(password, agent.password) &&
                Objects.equals(pin, agent.pin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, msisdn, password, pin);
    }

    @Override
    public String toString() {
        return "Agent{" +
                "id='" + id + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", password='" + password + '\'' +
                ", pin='" + pin + '\'' +
                '}';
    }
}
