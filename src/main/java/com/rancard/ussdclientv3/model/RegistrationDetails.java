package com.rancard.ussdclientv3.model;

import java.util.HashMap;
import java.util.Map;

public class RegistrationDetails {

    public RegistrationDetails(Session session) {
        this.product = session.getClientProperties().getProduct();
        this.firstName = session.getClientProperties().getFirstName();
        this.lastName = session.getClientProperties().getLastName();
        this.otherNames = session.getClientProperties().getOtherName();
        this.phoneNumber = session.getClientProperties().getFirstName();
        this.idType = session.getClientProperties().getIdType();
        this.idNumber = session.getClientProperties().getIdNumber();
        this.paymentMedium = session.getClientProperties().getPaymentMedium();
        this.amount = session.getClientProperties().getAmount();
        this.firstName = session.getClientProperties().getFirstName();
    }

    private Session session;

    private PRODUCT product;

    private String firstName;

    private String lastName;

    private String otherNames;

    private String phoneNumber;

    private ID idType;

    private String idNumber;

    private PAYMENT_MEDIUM paymentMedium;

    private String amount;


    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public PRODUCT getProduct() {
        return product;
    }

    public void setProduct(PRODUCT product) {
        this.product = product;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ID getIdType() {
        return idType;
    }

    public void setIdType(ID idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public PAYMENT_MEDIUM getPaymentMedium() {
        return paymentMedium;
    }

    public void setPaymentMedium(PAYMENT_MEDIUM paymentMedium) {
        this.paymentMedium = paymentMedium;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = session.getClientProperties().getFirstName();;
    }

    public String getFirstName() {
        return firstName;
    }


    public Map<String,Object> toMap (){
        Map<String,Object> registrationDetails = new HashMap<>();
        registrationDetails.put("product", this.product.toString());
        registrationDetails.put("firstName", this.firstName);
        registrationDetails.put("lastName", this.lastName);
        registrationDetails.put("otherNames", this.otherNames);
        registrationDetails.put("idType", this.idType.toString());
        registrationDetails.put("idNumber", this.idNumber);
        registrationDetails.put("paymentMedium", this.paymentMedium.toString());
        registrationDetails.put("amount", this.amount);
        return registrationDetails;
    }
}
