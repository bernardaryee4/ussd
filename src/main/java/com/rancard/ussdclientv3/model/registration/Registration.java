package com.rancard.ussdclientv3.model.registration;

import com.google.gson.JsonObject;
import com.rancard.ussdclientv3.model.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class Registration {

    private String firstName;
    private String lastName;
    private String otherName;
    private String product;
    private String amount;
    private String msisdn;
    private String paymentMedium;
    private String idType;
    private String idNumber;
    private String agentPhone;

    private Logger logger = LoggerFactory.getLogger(Registration.class);

    public Registration() {
    }

    public Registration(Client client) {
        this.firstName = client.getFirstName();
        this.lastName = client.getLastName();
        this.otherName = client.getOtherName() != null ? client.getOtherName() : "";
        this.product = client.getProduct().toString();
        this.amount = client.getAmount();
        this.paymentMedium = client.getPaymentMedium().toString();
        this.idType = client.getIdType().toString();
        this.msisdn = client.getMsisdn();
        this.idNumber = client.getIdNumber();
    }

    public Registration(String firstName, String lastName, String otherName, String product, String amount, String paymentMedium, String idType, String idNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.otherName = otherName;
        this.product = product;
        this.amount = amount;
        this.paymentMedium = paymentMedium;
        this.idType = idType;
        this.idNumber = idNumber;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentMedium() {
        return paymentMedium;
    }

    public void setPaymentMedium(String paymentMedium) {
        this.paymentMedium = paymentMedium;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getAgentPhone() {
        return agentPhone;
    }

    public void setAgentPhone(String agentPhone) {
        this.agentPhone = agentPhone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registration that = (Registration) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(otherName, that.otherName) &&
                Objects.equals(product, that.product) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(msisdn, that.msisdn) &&
                Objects.equals(paymentMedium, that.paymentMedium) &&
                Objects.equals(idType, that.idType) &&
                Objects.equals(idNumber, that.idNumber) &&
                Objects.equals(agentPhone, that.agentPhone) &&
                Objects.equals(logger, that.logger);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, otherName, product, amount, msisdn, paymentMedium, idType, idNumber, agentPhone, logger);
    }

    @Override
    public String toString() {
        return "Registration{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", otherName='" + otherName + '\'' +
                ", product='" + product + '\'' +
                ", amount='" + amount + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", paymentMedium='" + paymentMedium + '\'' +
                ", idType='" + idType + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", agentPhone='" + agentPhone + '\'' +
                '}';
    }

    public JsonObject toDoRegistrationParams(){

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("agentPhone",this.agentPhone);
        jsonObject.addProperty("clientPhone",this.msisdn);
        jsonObject.addProperty("amount",this.amount);
        jsonObject.addProperty("firstName",this.firstName);
        jsonObject.addProperty("lastName",this.lastName);
        jsonObject.addProperty("otherName",this.otherName);
        jsonObject.addProperty("paymentMedium",paymentMedium);
        jsonObject.addProperty("productType",this.product);
        jsonObject.addProperty("clientIdType",this.idType);
        jsonObject.addProperty("clientIdNumber",this.idNumber);

        logger.info("Json object response : "+ jsonObject);
        return jsonObject;
    }
}
