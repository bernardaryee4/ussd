package com.rancard.ussdclientv3.model;

import java.util.Arrays;

public enum MENU_OPTION {
    ZERO("0","ZERO"),
    ONE("1","ONE"),
    TWO("2","TWO"),
    THREE("3","THREE"),
    FOUR("4","FOUR"),
    FIVE("5","FIVE"),
    SIX("6","SEVEN"),
    HASH("#","HASH");

    public String choice;
    public String name;

    MENU_OPTION(String choice, String name){
        this.choice = choice;
        this.name = name;
    }

    public static MENU_OPTION fromChoice(String choice){
        MENU_OPTION menuOption = null;
        if(choice !=null){
            menuOption = Arrays.stream(MENU_OPTION.values()).filter(c -> { return choice.equalsIgnoreCase(c.choice); })
                    .findFirst().orElse(null);
        }
        return menuOption;
    }

}
