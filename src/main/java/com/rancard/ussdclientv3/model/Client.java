package com.rancard.ussdclientv3.model;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;
import java.util.Objects;

public class Client {

    private String msisdn;
    private String firstName;
    private String lastName;
    private PRODUCT product;
    private String amount;
    private PAYMENT_MEDIUM paymentMedium;
    private String otherName;
    private ID idType;
    private DEPOSIT_ID depositId;
    private String depositIdNumber;
    private String idNumber;
    private Map<Integer,String> accountMap;
    private String accounts;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public PRODUCT getProduct() {
        return product;
    }

    public void setProduct(PRODUCT product) {
        this.product = product;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public PAYMENT_MEDIUM getPaymentMedium() {
        return paymentMedium;
    }

    public void setPaymentMedium(PAYMENT_MEDIUM paymentMedium) {
        this.paymentMedium = paymentMedium;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public ID getIdType() {
        return idType;
    }

    public void setIdType(ID idType) {
        this.idType = idType;
    }

    public DEPOSIT_ID getDepositId() {
        return depositId;
    }

    public void setDepositId(DEPOSIT_ID depositId) {
        this.depositId = depositId;
    }

    public String getDepositIdNumber() {
        return depositIdNumber;
    }

    public void setDepositIdNumber(String depositIdNumber) {
        this.depositIdNumber = depositIdNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Map<Integer, String> getAccountMap() {
        return accountMap;
    }

    public void setAccountMap(Map<Integer, String> accountMap) {
        this.accountMap = accountMap;
    }

    public String getAccounts() {
        return accounts;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(msisdn, client.msisdn) &&
                Objects.equals(firstName, client.firstName) &&
                Objects.equals(lastName, client.lastName) &&
                product == client.product &&
                Objects.equals(amount, client.amount) &&
                paymentMedium == client.paymentMedium &&
                Objects.equals(otherName, client.otherName) &&
                idType == client.idType &&
                depositId == client.depositId &&
                Objects.equals(depositIdNumber, client.depositIdNumber) &&
                Objects.equals(idNumber, client.idNumber) &&
                Objects.equals(accountMap, client.accountMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msisdn, firstName, lastName, product, amount, paymentMedium, otherName, idType, depositId, depositIdNumber, idNumber, accountMap);
    }

    @Override
    public String toString() {
        return "Client{" +
                "msisdn='" + msisdn + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", product=" + product +
                ", amount='" + amount + '\'' +
                ", paymentMedium=" + paymentMedium +
                ", otherName='" + otherName + '\'' +
                ", idType=" + idType +
                ", depositId=" + depositId +
                ", depositIdNumber='" + depositIdNumber + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", accountMap=" + accountMap +
                '}';
    }

}
