package com.rancard.ussdclientv3.model;

import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.Objects;


@RedisHash
public class Session implements Serializable {

    private String id;
    private String menuLevel;
    private String subMenuLevel;
    private Agent agentProperties;
    private Client clientProperties;


    public Session(){

    }

    public Session(String id, String menuLevel) {
        this.id = id;
        this.menuLevel = menuLevel;
    }

    public Session(String id, String menuLevel,String subMenuLevel) {
        this.id = id;
        this.menuLevel = menuLevel;
        this.subMenuLevel = subMenuLevel;
    }

    public Session(String id, String menuLevel, Agent agentProperties) {
        this.id = id;
        this.menuLevel = menuLevel;
        this.agentProperties = agentProperties;
    }

    public Session(String id, String menuLevel, Agent agentProperties, Client clientProperties) {
        this.id = id;
        this.menuLevel = menuLevel;
        this.subMenuLevel = subMenuLevel;
        this.agentProperties = agentProperties;
        this.clientProperties = clientProperties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(String menuLevel) {
        this.menuLevel = menuLevel;
    }

    public String getSubMenuLevel() {
        return subMenuLevel;
    }

    public void setSubMenuLevel(String subMenuLevel) {
        this.subMenuLevel = subMenuLevel;
    }

    public Agent getAgentProperties() {
        return agentProperties;
    }

    public void setAgentProperties(Agent agentProperties) {
        this.agentProperties = agentProperties;
    }

    public Client getClientProperties() {
        return clientProperties;
    }

    public void setClientProperties(Client clientProperties) {
        this.clientProperties = clientProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return Objects.equals(id, session.id) &&
                Objects.equals(menuLevel, session.menuLevel) &&
                Objects.equals(subMenuLevel, session.subMenuLevel) &&
                Objects.equals(agentProperties, session.agentProperties) &&
                Objects.equals(clientProperties, session.clientProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, menuLevel, subMenuLevel, agentProperties, clientProperties);
    }

    @Override
    public String toString() {
        return "Session{" +
                "id='" + id + '\'' +
                ", menuLevel='" + menuLevel + '\'' +
                ", subMenuLevel='" + subMenuLevel + '\'' +
                ", agentProperties=" + agentProperties +
                ", clientProperties=" + clientProperties +
                '}';
    }
}
