package com.rancard.ussdclientv3.model;

import java.util.Arrays;

public enum PRODUCT {
    AHONYA("1","AHONYA"),
    SISF("2","SISF"),
    IMA("3","IMA"),
    TBILL("4","TBILL"),
    HORIZON("5","HORIZON");

    private String choice;
    private String name;

    PRODUCT(String choice, String name){
        this.choice = choice;
        this.name = name;
    }

    public static PRODUCT fromChoice(String choice){
        PRODUCT product = null;
        if(choice !=null){
            product = Arrays.stream(PRODUCT.values()).filter(c -> { return choice.equalsIgnoreCase(c.choice); })
                    .findFirst().orElse(null);
        }
        return product;
    }

    public static String initialDeposit(PRODUCT accountType) {
        switch (accountType) {
            case AHONYA:
            case IMA:
                return "10";
            case SISF:
            case TBILL:
                return "1";
            case HORIZON:
                return "50";
            default:
                return null;
        }
    }

}
