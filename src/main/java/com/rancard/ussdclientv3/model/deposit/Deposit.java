package com.rancard.ussdclientv3.model.deposit;

import com.google.gson.JsonObject;
import com.rancard.ussdclientv3.model.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class Deposit {
    private String agentId;
    private String agentPhone;
    private String productType;
    private String clientId;
    private String clientIdNumber;
    private String paymentMedium;
    private String paymentId;
    private String accountNumber;
    private String amount;


    private Logger logger = LoggerFactory.getLogger(Deposit.class);

    public Deposit() {
    }

    public Deposit(Client client) {
        this.productType = client.getProduct().toString();
        this.clientId= client.getDepositId().toString();
        this.clientIdNumber = client.getDepositIdNumber();
        this.paymentMedium = client.getPaymentMedium().toString();
        this.amount = client.getAmount();
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientIdNumber() {
        return clientIdNumber;
    }

    public void setClientIdNumber(String clientIdNumber) {
        this.clientIdNumber = clientIdNumber;
    }

    public String getPaymentMedium() {
        return paymentMedium;
    }

    public void setPaymentMedium(String paymentMedium) {
        this.paymentMedium = paymentMedium;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAgentPhone() {
        return agentPhone;
    }

    public void setAgentPhone(String agentPhone) {
        this.agentPhone = agentPhone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deposit deposit = (Deposit) o;
        return Objects.equals(agentId, deposit.agentId) &&
                Objects.equals(productType, deposit.productType) &&
                Objects.equals(clientId, deposit.clientId) &&
                Objects.equals(clientIdNumber, deposit.clientIdNumber) &&
                Objects.equals(paymentMedium, deposit.paymentMedium) &&
                Objects.equals(paymentId, deposit.paymentId) &&
                Objects.equals(accountNumber, deposit.accountNumber) &&
                Objects.equals(agentPhone, deposit.agentPhone) &&
                Objects.equals(amount, deposit.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agentId,agentPhone, productType, clientId, clientIdNumber, paymentMedium, paymentId, accountNumber, amount);
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "agentId='" + agentId + '\'' +
                "agentPhone='" + agentPhone + '\'' +
                ", productType='" + productType + '\'' +
                ", clientId='" + clientId + '\'' +
                ", clientIdNumber='" + clientIdNumber + '\'' +
                ", paymentMedium='" + paymentMedium + '\'' +
                ", paymentId='" + paymentId + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }

    public JsonObject toDoDepositParams(){

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("agentId",this.agentId);
        jsonObject.addProperty("clientIdNumber",this.clientIdNumber);
        jsonObject.addProperty("clientIdType",this.clientId);
        jsonObject.addProperty("productType",this.productType);
        jsonObject.addProperty("amount",this.amount);
        jsonObject.addProperty("paymentId","");
        jsonObject.addProperty("paymentMedium",this.paymentMedium);
        jsonObject.addProperty("personnelPhone",this.agentPhone);


        logger.info("Json object response : "+ jsonObject);
        return jsonObject;
    }
}
