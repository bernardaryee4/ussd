package com.rancard.ussdclientv3.model;

import java.util.Arrays;

public enum PAYMENT_MEDIUM {
    CASH("1", "CASH"),MOBILE_MONEY("2", "MOBILE_MONEY"), CHEQUE("3","CHEQUE");

    private String choice;
    private String name;

    PAYMENT_MEDIUM(String choice, String name){
        this.choice = choice;
        this.name = name;
    }

    public static PAYMENT_MEDIUM fromChoice(String choice){
        PAYMENT_MEDIUM paymentMedium = null;
        if(choice !=null){
            paymentMedium = Arrays.stream(PAYMENT_MEDIUM.values()).filter(c -> { return choice.equalsIgnoreCase(c.choice); })
                    .findFirst().orElse(null);
        }
        return paymentMedium;
    }

}
