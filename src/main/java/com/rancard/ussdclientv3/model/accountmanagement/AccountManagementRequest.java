package com.rancard.ussdclientv3.model.accountmanagement;

import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AccountManagementRequest {

    private String msisdn;
    private String pin;
    private String password;

    private Logger logger = LoggerFactory.getLogger(AccountManagementRequest.class);

    public AccountManagementRequest() {

    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountManagementRequest that = (AccountManagementRequest) o;
        return Objects.equals(msisdn, that.msisdn) &&
                Objects.equals(pin, that.pin) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msisdn, pin, password);
    }

    @Override
    public String toString() {
        return "AccountManagementRequest{" +
                "msisdn='" + msisdn + '\'' +
                ", pin='" + pin + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public Map<String,Object> toGetPinRequestParams(){
        Map<String,Object> map = new HashMap<>();

        map.put("agentPhone",this.msisdn);
        map.put("pin",this.pin);

        return map;
    }

    public JsonObject toValidatePasswordRequestParams(){

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("msisdn",this.msisdn);
        jsonObject.addProperty("password",this.password);

        return jsonObject;
    }

    public JsonObject toValidatePinRequestParams(){

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("msisdn",this.msisdn);
        jsonObject.addProperty("pin",this.pin);

        return jsonObject;
    }

    public Map<String,Object> toMap(){
        Map<String,Object> map = new HashMap<>();

        map.put("phone",this.msisdn);
        map.put("pin",this.pin);
        map.put("password",this.password);

        return map;
    }

    public JsonObject toSetPinRequestParams(){

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("msisdn",this.msisdn);
        jsonObject.addProperty("pin",this.pin);

        logger.info("Json object response : "+ jsonObject);
        return jsonObject;
    }
}
