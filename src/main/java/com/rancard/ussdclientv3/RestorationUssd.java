package com.rancard.ussdclientv3;

import com.rancard.ussdclientv3.model.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;


//@PropertySource("classpath:conf/${SPRING_PROFILES_ACTIVE}.properties")
@SpringBootApplication
public class RestorationUssd {

    private Logger logger = LoggerFactory.getLogger(RestorationUssd.class);

    @Value(value="${spring.redis.host}")
    String REDIS_HOST;

    @Value(value = "${spring.redis.port}")
    Integer REDIS_PORT;

    @Bean
    JedisConnectionFactory jedisConnectionFactory(){
        logger.info("Connection properties for Jedis : hostname :: "+REDIS_HOST+" port :: "+REDIS_PORT);
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName(REDIS_HOST);
        jedisConnectionFactory.setPort(REDIS_PORT);

        return jedisConnectionFactory;
    }

    @Bean
    RedisTemplate<String, Session> redisTemplate(){
        RedisTemplate<String, Session> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(jedisConnectionFactory());
        return redisTemplate;
    }

	public static void main(String[] args) {
		SpringApplication.run(RestorationUssd.class, args);
	}

}
