package com.rancard.ussdclientv3.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    //todo --- Implement regex for phone validation
    public static boolean isValidMsisdn(String msisdn){
        return msisdn.matches("(00233|0233|\\+233|233|0)(23|24|54|55|27|57|28|20|50|26|56|30|31|32|33|34|35|37|38|39)\\d{7}");

    }

    public static Map<String, String> checkAndReturnIdentification(String identification) {
        Map<String, String> identifier = new HashMap<>();
        Pattern validPhone = Pattern.compile("(00233|0233|\\+233|233|0)(23|24|54|55|27|57|28|20|50|26|56)\\d{7}");
        Pattern validEmail = Pattern.compile("^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher phoneMatcher = validPhone.matcher(identification);
        Matcher emailMatcher = validEmail.matcher(identification);

        if (phoneMatcher.matches()) {
            identifier.put("isValid", String.valueOf(true));
            identifier.put("clientIdType", "1");
            identifier.put("clientIdNo", toValidNumberFormat(identification));
        } else if (emailMatcher.matches()) {
            identifier.put("isValid", String.valueOf(true));
            identifier.put("clientIdType", "2");
            identifier.put("clientIdNo", identification);
        } else {
            identifier.put("isValid", String.valueOf(true));
            identifier.put("clientIdType", "3");
            identifier.put("clientIdNo",identification);
        }
        return identifier;
    }

    public static String toValidNumberFormat(String number) {
        switch (number.trim().length()) {
            case 10:
                return number;
            case 13:
                return number.replaceFirst("^\\+233+(?!$)", "0");
            case 12:
                return number.replaceFirst("^233+(?!$)", "0");
            default:
                return null;
        }
    }

    public static String toValidInternationalNumberFormat(String msisdn){

        if(msisdn.startsWith("00233")){
            msisdn= msisdn.substring(2);
        } else if(msisdn.startsWith("0233")){
            msisdn = msisdn.substring(1);
        } else if(msisdn.startsWith("+233")){
            msisdn = msisdn.substring(1);
        } else if(msisdn.startsWith("0")){
            msisdn = "233" + msisdn.substring(1);
        }else{
            return msisdn;
        }
        return msisdn;

    }
}
