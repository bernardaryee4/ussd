package com.rancard.ussdclientv3.controller;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class UssdResponse {

    private String message;
    private boolean continueSession;

    public UssdResponse(String message, boolean continueSession) {
        this.message = message;
        this.continueSession = continueSession;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isContinueSession() {
        return continueSession;
    }

    public void setContinueSession(boolean continueSession) {
        this.continueSession = continueSession;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UssdResponse that = (UssdResponse) o;
        return continueSession == that.continueSession &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, continueSession);
    }

    @Override
    public String toString() {
        return "UssdResponse{" +
                "message='" + message + '\'' +
                ", continueSession=" + continueSession +
                '}';
    }

    public String toUnifiedResponse(){
        Map<String, Object> unifiedResponse = new HashMap<>();
        unifiedResponse.put("message",this.message);
        unifiedResponse.put("continueSession",this.continueSession);

        return new Gson().toJson(unifiedResponse);
    }
}
