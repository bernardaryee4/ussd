package com.rancard.ussdclientv3.controller;

import com.rancard.ussdclientv3.model.MENU_OPTION;
import com.rancard.ussdclientv3.utils.Utils;

import java.util.Objects;

public class UssdRequest {

  private String sessionId;
  private String msisdn;
  private String mobileNetwork;
  private String message;
  private String menu;

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = Utils.toValidInternationalNumberFormat(msisdn);
  }

  public String getMobileNetwork() {
    return mobileNetwork;
  }

  public void setMobileNetwork(String mobileNetwork) {
    this.mobileNetwork = mobileNetwork;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getMenu() {
    return menu;
  }

  public void setMenu(String menu) {
    this.menu = menu;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UssdRequest that = (UssdRequest) o;
    return Objects.equals(sessionId, that.sessionId)
        && Objects.equals(msisdn, that.msisdn)
        && Objects.equals(mobileNetwork, that.mobileNetwork)
        && Objects.equals(message, that.message)
        && Objects.equals(menu, that.menu);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionId, msisdn, mobileNetwork, message, menu);
  }

  @Override
  public String toString() {
    return "UssdRequest{"
        + "sessionId='"
        + sessionId
        + '\''
        + ", msisdn='"
        + msisdn
        + '\''
        + ", mobileNetwork='"
        + mobileNetwork
        + '\''
        + ", message='"
        + message
        + '\''
        + ", menu='"
        + menu
        + '\''
        + '}';
  }

  boolean isValidRequest() {
    return (this.menu != null
        && this.message != null
        && this.mobileNetwork != null
        && this.msisdn != null
        && this.sessionId != null);
  }

  public MENU_OPTION getMenuOption(){
    System.out.println("get menu option "+ this.message);
    return MENU_OPTION.valueOf(this.message);
  }
}
