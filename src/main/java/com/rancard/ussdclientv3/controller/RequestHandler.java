package com.rancard.ussdclientv3.controller;

import com.rancard.ussdclientv3.model.Session;
import com.rancard.ussdclientv3.service.RequestDispatcherService;
import com.rancard.ussdclientv3.service.accountmanagement.AccountManagementService;
import com.rancard.ussdclientv3.service.sessionmanagement.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
public class RequestHandler {

  private Logger logger = LoggerFactory.getLogger(RequestHandler.class);

  private AccountManagementService accountManagementService;
  private SessionManager sessionManager;
  private RequestDispatcherService requestDispatcherService;

  @Autowired
  public RequestHandler(RequestDispatcherService requestDispatcherService, SessionManager sessionManager, AccountManagementService accountManagementService) {
    this.accountManagementService = accountManagementService;
    this.sessionManager = sessionManager;
    this.requestDispatcherService = requestDispatcherService;
  }

  @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public String validateUssdRequest(
      @RequestBody UssdRequest ussdRequest, HttpServletRequest request) {

    String sessionId = request.getSession().getId();
    UssdResponse response = new UssdResponse(null,true);

    logger.info("[" + sessionId + "] about to validate request : " + ussdRequest.toString());

    boolean isValidRequest = ussdRequest.isValidRequest();

    if (isValidRequest) {

      String ussdSessionId = ussdRequest.getSessionId() + "@" + ussdRequest.getMobileNetwork();

      logger.info("[" + sessionId + "] checking existing sessions for : " + ussdSessionId);

      Session currentSession = sessionManager.getSessionById(ussdSessionId);

      if (ussdRequest.getMenu().equals("0")) {

          currentSession = new Session(ussdSessionId,"init");
          sessionManager.persistSession(currentSession,sessionId);
          logger.info("[" + sessionId + "] initial request found");

          response = accountManagementService.checkAgentExistence(ussdRequest, sessionId,currentSession,response);

      }else{
          logger.info("Retrieving old session menu for session id :: "+ currentSession.getId());
          String menu = currentSession.getMenuLevel();
          response = requestDispatcherService.processRequest(ussdRequest,menu,currentSession,sessionId);

      }
        sessionManager.persistSession(currentSession,sessionId);

    }
    return response.toUnifiedResponse();
  }
}
